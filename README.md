
## Benchmark 

The benchmark has been performed on a CPU with 4 physical cores each core with 2 threads. 
The reason why I choose **16** as the size pool is that: 
Even though we wouldn't have 16 concurrent processes, knowing that the task has IO blocking operations, providing larger pool  than the exact number of cpus, would allow context-switching 
between idle processes (during IO operations).     


## Limitation 
* If the processing times of each image are not equivalent, the implementation does not take advantage of that. A better approach would be to use an inter process queue to put and get the images as soon as they get ready. As current implementation put and get images sequentially. 
* Larger cache means better performance but also more memory usage, cache size would be chosen depending on the requirements and the limitations.
* To go further, if we have control over the code that processes the image. I would try to deal with IO blocking operations using python multithreading and then continue to multiprocess CPU operations. This would result in a smaller pool and less context switching and therefore better performance. (Although it should be tested)     
* The dataset size should be a multiple of cache batch size if not an out of range index error may be raised. To solve this the remaining indices should be treated in the last increment.

## Result 

```
optimized:
+------------+-----------------------+
| batch_size | duration (in seconds) |
+------------+-----------------------+
|     1      |         0.5172        |
|     10     |         0.6027        |
|     20     |         0.6143        |
+------------+-----------------------+

```
