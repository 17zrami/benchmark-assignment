from prettytable import PrettyTable
from time import sleep, time
import numpy as np
from multiprocessing import Pool
from queue import Queue

HEIGHT = 100
WIDTH = 100
CHANNELS = 1
NB_IMAGES = 200


def simulate_io_work():
    """
    DO NOT EDIT

    Put current thread to sleep, as if it were waiting for a file system response.
    """
    sleep(0.01)


def simulate_cpu_work():
    """
    DO NOT EDIT

    Feed current thread with stupid but cpu-demanding work.
    """
    for i in range(200_000):
        i % 7


class Dataset:
    """
    DO NOT EDIT

    Simple class to interact with a Dataset.

    The __getitem__ method allows to access dataset items with square brackets:
    >>> dataset = Dataset()
    >>> dataset[0]
    >>> dataset[1]
    """

    def __len__(self):
        """
        Returns the length of the dataset.
        This allows to call python built-in method len() over a Dataset instance.
        """
        return NB_IMAGES

    def __getitem__(self, i):
        """
        Returns the i-th item of the dataset.

        The returned value is a tuple with 2 elements:
            - index i: a numpy array of shape (1, 1)
            - corresponding image: a numpy array of shape (1, HEIGHT, WIDTH, CHANNELS)

        For the sake of the exercise, the image is set to random noise.

        For the sake of the exercise, some fake io-work and cpu-work are performed
        each time a dataset item is accessed.
        """

        if i >= len(self):
            # We have reached end of the dataset
            raise IndexError

        simulate_io_work()
        simulate_cpu_work()

        idx = np.array(i).reshape((1, 1))
        image = np.random.uniform(size=(1, HEIGHT, WIDTH, CHANNELS))

        return idx, image


class Batcher:
    """
    DO NOT EDIT

    Base class for batchers. A batcher is supposed to:
        - take as input a `dataset` and a `batch_size`
        - iterate over the dataset items
        - group these items into batches (of size `batch_size`)
        - yield batches as they are ready

    A typical batch should be a tuple with 2 elements:
            - indexes: a numpy array of shape (batch_size, 1)
            - images: a numpy array of shape (batch_size, HEIGHT, WIDTH, CHANNELS)

    For any given i: images[i] should be the image corresponding to indexes[i].
    """

    def __init__(self, dataset, batch_size):
        self.dataset = dataset
        self.batch_size = batch_size
        assert (
                len(self.dataset) % self.batch_size == 0
        ), "Dataset length should be a multiple of the batch size"

    def __iter__(self):
        # This method should be implemented in child classes
        raise NotImplementedError

    def __next__(self):
        # This method should be implemented in child classes
        raise NotImplementedError


class NaiveBatcher(Batcher):
    """
    DO NOT EDIT

    A naive but working implementation for batcher.

    This NaiveBatcher does implement the iterator protocol (__iter__ and
    __next__ methods).

    Batches are built sequentially, on-the-fly, as they are requested.
    This is probably sub-optimal in terms of code speed!
    """

    def __iter__(self):
        # initiate the iterator
        self.batch_idx = 0
        self.max_batch_idx = len(self.dataset) // self.batch_size
        return self

    def __next__(self):

        # stop iterating if all batches have been delivered
        if self.batch_idx == self.max_batch_idx:
            raise StopIteration

        # create next batch
        indexes = list()
        images = list()
        for i in range(self.batch_size):
            # fetch dataset item
            dataset_idx = self.batch_idx * self.batch_size + i
            idx, image = self.dataset[dataset_idx]
            indexes.append(idx)
            images.append(image)

        # stack items into a batch
        indexes = np.concatenate(indexes)
        images = np.concatenate(images)

        # update current batch index before returning freshly created batch
        self.batch_idx += 1

        return indexes, images


class FastBatcher(Batcher):

    @staticmethod
    def process(dataset, dataset_idx):
        """ Process a single image """
        return dataset[dataset_idx]

    def __init__(self, *args, **kwargs):
        super(FastBatcher, self).__init__(*args, **kwargs)
        self.pool = Pool(16)
        self.cache_idx = 0
        self.cache_batch_size = 20
        assert (
                len(self.dataset) % self.cache_batch_size == 0
        ), "Dataset length should be a multiple of the cache batch size"
        self.max_cache_idx = len(self.dataset) // self.cache_batch_size
        self.cache = Queue()

    def __iter__(self):
        # initiate the iterator
        self.batch_idx = 0
        self.max_batch_idx = len(self.dataset) // self.batch_size
        return self

    def cache_next(self):
        """ Fetch next cache_batch_size elements and store them  """

        if self.cache_idx == self.max_cache_idx:
            raise Exception("No more elements to fetch")

        # submit tasks to pool
        for i in range(self.cache_batch_size):
            self.cache.put(
                self.pool.apply_async(FastBatcher.process, (self.dataset, self.cache_idx * self.cache_batch_size + i)))

        self.cache_idx += 1

    def __next__(self):
        # stop iterating if all batches have been delivered
        if self.batch_idx == self.max_batch_idx:
            raise StopIteration

        # fetch result when necessary
        while self.cache.qsize() < self.batch_size:
            self.cache_next()

        # stack items into a batch
        results = [self.cache.get().get() for i in range(self.batch_size)]
        indexes, images = zip(*results)

        indexes = np.concatenate(indexes)
        images = np.concatenate(images)

        # update current batch index before returning freshly created batch
        self.batch_idx += 1

        return indexes, images


def benchmark(batcher_class, batch_sizes=(1, 10, 20)):
    """
    DO NOT EDIT

    This is an helper function to perform benchmark profiling on Batcher implementations.
    The provided 'batcher_class' should be a child class of Batcher.

    The benchmark is performed for different batch sizes and simply consist in measuring
    the amount of time required the iterate over the complete batched dataset.
    """

    durations = dict()

    for batch_size in batch_sizes:

        # profile batcher for current batch_size

        expected_indexes_shape = (batch_size, 1)
        expected_images_shape = (batch_size, HEIGHT, WIDTH, CHANNELS)

        # init batcher and iterate over it
        start = time()
        seen_indexes = list()
        batcher = batcher_class(dataset=Dataset(), batch_size=batch_size)

        for batch in batcher:
            indexes, images = batch

            indexes_shape = indexes.shape
            assert (
                    indexes_shape == expected_indexes_shape
            ), f"indexes have wrong shape: {indexes_shape} != {expected_indexes_shape}"

            images_shape = images.shape
            assert (
                    images_shape == expected_images_shape
            ), f"images have wrong shape: {images_shape} != {expected_images_shape}"

            seen_indexes.extend([int(idx) for idx in indexes])

        # make sure the batcher has properly covered all item in the dataset
        assert sorted(seen_indexes) == list(range(NB_IMAGES))

        stop = time()
        duration = stop - start
        durations[batch_size] = duration

    # display table
    table = PrettyTable(field_names=["batch_size", "duration (in seconds)"])
    for batch_size, duration in durations.items():
        table.add_row([batch_size, f"{duration:.4f}"])
    print(table)


if __name__ == "__main__":
    print("naive:")
    benchmark(NaiveBatcher)
    print("optimized:")
    benchmark(FastBatcher)
