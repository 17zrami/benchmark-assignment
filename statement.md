# Take-home exercise [SWE at Therapixel]

## Goal

Evaluate candidate's capacity to produce quality code, that solves a real technical issue, under time constraint.

## Problem statement

The exercise takes the form of a python script `benchmark.py`. Inside this script, you will find:
* a `Dataset` class
* a `Batcher` abstract class
* an example of implementation `NaiveBatcher`
* a profiling function `benchmark`

When running the script `benchmark.py`, your console should display something like:
```
% python3.7 benchmark.py
naive:
+------------+-----------------------+
| batch_size | duration (in seconds) |
+------------+-----------------------+
|     1      |         5.1464        |
|     10     |         4.8730        |
|     20     |         4.9601        |
+------------+-----------------------+
```
> To properly run this script, you need a python version >= 3.7 and the following packages: `prettytable`, `numpy`.

**Your goal is to implement a faster version of the batcher!**

Once finished, running the script `benchmark.py` should run profiling for the naive version and profiling for your optimized submission.
The optimized batcher is expected to run an order of magnitude faster than the naive one.
The console should display something that looks like:
```
naive:
+------------+-----------------------+
| batch_size | duration (in seconds) |
+------------+-----------------------+
|     1      |         4.9484        |
|     10     |         4.8901        |
|     20     |         4.9607        |
+------------+-----------------------+

optimized:
+------------+-----------------------+
| batch_size | duration (in seconds) |
+------------+-----------------------+
|     1      |         0.9996        |
|     10     |         1.0065        |
|     20     |         1.0273        |
+------------+-----------------------+
```

Of course, provided code for `Dataset`, `Batcher`, `NaiveBatcher`, `benchmark` and defined contants shoud NOT be changed.

## Evaluation

The exercise will be graded against a rigorous set of predetermined criteria.
    
When running the `benchmark.py` script, we expect to get profiling for the `NaiveBatcher` and profiling for the `FastBatcher` class you will implement.

In the submitted code, we will be looking at (by order of importance):
1. Correctness
2. Cleanliness
3. Performance
4. Version tracking (we recommand you to create a private github project and commit your work there)
5. Code packaging (we recommand you to properly package the code, using `virtualenv` or `docker` for example)

Finally, you should explain limitations of your implementation, and describe possible options for improvement.

## Important notes

* We do not expect you to work more than 6h on this assignment.
* Goal is to work as efficiently as possible, and see what can be produced in a reasonable amount of time.
* When submitting your work, please indicate the time spent.
